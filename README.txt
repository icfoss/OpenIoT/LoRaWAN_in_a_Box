﻿LoRaWAN in a Box - Workshop Manual


Prior to Workshop
1. Configure Keyboard and Time.
2. Enable SSH
3. Enable Boot to Console
4. Enable WiFi 
   1. Set Region to EU
   2. SSID : ICFOSS
   3. PSK: 12344567888


1. Check Network Connectivity. Run the following Script:


sudo apt-get update
sudo apt-get update --fix-missing
sudo apt install git


1. Copy Eth0 and WLAN0 MAC Address. 
2. Change Hostname : LIB-x , Eg : LIB-1
3. Change password : LIB-x, same as HostName
4. Print Hostname, MAC Address (Eth0 and Wlan0) 


Test : Login to Pi using SSH from a Remote Machine


________________




Workshop :
git clone https://gitlab.com/icfoss/OpenIoT/LoRaWAN_in_a_Box.git


sudo ./install.sh


Copy Gateway EUI


sudo reboot 


After Restart, RAK 833 LED should light up


sudo gateway-config


Setup Concentrator
   1. Server : LoRa Server
   2. EU868
   3. IP : 127.0.0.1


Login to Page : IP Address:8080
User : admin
Pass : admin


Delete Gateway
Add Gateway


Add Device Profile
Create Device


Write Decoder


function Decode(fPort, bytes)
{
  var decoded = {};
 
  decoded.temperature = bytes[0];
  decoded.humidity = bytes[2];
  return decoded;


}




________________




sudo ./install_grafana_influx.sh


crontab -e


Add


@reboot python3 /home/pi/mqtt_data.py




________________






Influx


IP Address : 8083
Username : admin
Pass : admin


1. Query Templates
   1. Show Databases
   2. Create Database : “databasein”
1. Select Database : Top Right Option
2. Query Templates 
   1. Show Measurements
   2. App model should be Created now !


________________




Grafana


Ip address: 3000


Username : admin
Pass : admin


1. Add Data Source
   1. Name : influxdb
   2. Type : InfluxDB
   3. URL : http://localhost:8086
   4. Database : databasein
1. New Dashboard
   1. Graph
   2. Panel Title : edit
      1. Data Source : influxdb
      2. Select Measurement : AppModel
      3. Select : Field
      4. Save and View Dashboard